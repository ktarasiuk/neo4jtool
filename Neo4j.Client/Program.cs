﻿namespace Neo4j.Client
{
    using System;

    using Neo4j.Tool.Database;
    using Neo4j.Tool.Helpers;
    using Neo4j.Tool.Traversal;

    class Program
    {
        static void Main(string[] args)
        {
            DatabaseContext context = new DatabaseContext("http://localhost:7474");

            //var fred = context.CreateNode();
            //fred.AddProperty("FirstName", "Fryderyk");
            //fred.AddProperty("LastName", "Flinston");
            //fred.Label.Add("Person");

            //var wilma = context.CreateNode();
            //wilma.AddProperty("FirstName", "Wilma");
            //wilma.AddProperty("LastName", "Flinston");
            //wilma.Label.Add("Person");

            //var pebbles = context.CreateNode();
            //pebbles.AddProperty("FirstName", "Pebbles");
            //pebbles.AddProperty("LastName", "Flinston");
            //pebbles.Label.Add("Person");

            //var barney = context.CreateNode();
            //barney.AddProperty("FirstName", "Barney");
            //barney.AddProperty("LastName", "Rubble");
            //barney.Label.Add("Person");

            //var betty = context.CreateNode();
            //betty.AddProperty("FirstName", "Betty");
            //betty.AddProperty("LastName", "Rubble");
            //betty.Label.Add("Person");

            //var bamm = context.CreateNode();
            //bamm.AddProperty("FirstName", "Bamm-Bamm");
            //bamm.AddProperty("LastName", "Rubble");
            //bamm.Label.Add("Person");

            //var dino = context.CreateNode();
            //dino.AddProperty("FirstName", "Dino");
            //dino.AddProperty("LastName", "Flinston");
            //dino.AddProperty("Type", "Dog/Dinosaur");
            //dino.Label.Add("Pet");

            //var hoppy = context.CreateNode();
            //hoppy.AddProperty("FirstName", "Hoppy");
            //hoppy.AddProperty("LastName", "Rubble");
            //hoppy.AddProperty("Type", "Kangaroo/Dinosaur");
            //hoppy.Label.Add("Pet");

            //var slate = context.CreateNode();
            //slate.AddProperty("FirstName", "Slate");
            //slate.AddProperty("LastName", "Mr");
            //slate.Label.Add("Person");

            //context.CreateRelationship(fred, wilma, "Wife");
            //context.CreateRelationship(pebbles, fred, "Parent");
            //context.CreateRelationship(pebbles, wilma, "Parent");
            //context.CreateRelationship(barney, betty, "Wife");
            //context.CreateRelationship(bamm, barney, "Parent");
            //context.CreateRelationship(bamm, betty, "Parent");
            //context.CreateRelationship(bamm, dino, "Pet");
            //context.CreateRelationship(fred, dino, "Pet");
            //context.CreateRelationship(wilma, dino, "Pet");
            //context.CreateRelationship(barney, hoppy, "Pet");
            //context.CreateRelationship(betty, hoppy, "Pet");
            //context.CreateRelationship(bamm, hoppy, "Pet");
            //context.CreateRelationship(fred, barney, "Friend");
            //context.CreateRelationship(wilma, betty, "Friend");
            //context.CreateRelationship(bamm, pebbles, "Friend");
            //context.CreateRelationship(fred, slate, "Boss");

            //context.Commit();

            //var node = context.GetNodeById(5);

            //var query = context.Query();

            //query.Order(OrderType.depth_first)
            //     .Filter(new PropertyFilter().UserDefinedFilter("position.length()<=1;"));

            //var nodes = context.Traverse(node, query);

            Console.ReadKey();
        }
    }
}
