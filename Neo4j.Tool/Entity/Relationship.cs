﻿namespace Neo4j.Tool.Entity
{
    using System;

    using Neo4j.Tool.Parser;
    using Neo4j.Tool.Persistence;

    using Newtonsoft.Json.Linq;

    public class Relationship : BaseEntity
    {
        private string type;

        private readonly RelationshipRepository relationshipRepo;

        private readonly NodeRepository nodeRepo;

        public Node StartNode { get; private set; }

        public Node EndNode { get; private set; }

        private Relationship()
        {
            this.relationshipRepo = new RelationshipRepository();
            this.nodeRepo = new NodeRepository();
            base.SetRepository(this.relationshipRepo);
        }

        internal Relationship(Node startStartNode, Node endEndNode, string relationShipType)
            : this()
        {
            this.StartNode = startStartNode;
            this.EndNode = endEndNode;

            this.type = relationShipType;
        }

        internal Relationship(RequestResult result)
            : this()
        {
            ParserBase nodeParser = new RelationshipParser(this.keyValuePair);
            nodeParser.JsonToEntity(result, this);
        }

        internal override void SetLocation(Uri location)
        {
            if (location.ToString().Contains("node"))
            {
                throw new InvalidCastException(string.Format("Unable to cast Node to Relationship. The type is a Node, Location: {0}", location));
            }

            base.SetLocation(location);
        }

        public void SetType(string type)
        {
            this.type = type;
        }

        public new string GetType()
        {
            return this.type;
        }
        
        internal void SetVertices(string startNodePath, string endNodePath)
        {
            if (this.StartNode != null)
            {
                this.StartNode.SetLocation(new Uri(startNodePath));
            }
            else
            {
                this.StartNode = new Node(this.nodeRepo, this.relationshipRepo);
                this.StartNode.SetLocation(new Uri(startNodePath));
            }

            if (this.EndNode != null)
            {
                this.EndNode.SetLocation(new Uri(endNodePath));
            }
            else
            {
                this.EndNode = new Node(this.nodeRepo, this.relationshipRepo);
                this.EndNode.SetLocation(new Uri(endNodePath));
            }
        }
        
        public override string GetProperties()
        {
            JObject props = new JObject();
            props.Add("to", new JValue(this.EndNode.GetLocation()));
            props.Add("type", new JValue(this.type));

            return props.ToString();
        }
    }
}