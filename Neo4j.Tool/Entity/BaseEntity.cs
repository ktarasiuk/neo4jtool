﻿namespace Neo4j.Tool.Entity
{
    using System;
    using System.Collections.Generic;

    using Neo4j.Tool.Parser;
    using Neo4j.Tool.Persistence;

    using Net.Graph.Neo4JD.Exceptions;

    using Newtonsoft.Json.Linq;

    public class BaseEntity
    {
        protected readonly Dictionary<string, object> keyValuePair = new Dictionary<string, object>();

        private Uri location;

        private Repository baseRepo;

        protected void SetRepository(Repository repository)
        {
            this.baseRepo = repository;
        }

        public int Id
        {
            get
            {
                return this.GetId();
            }
        }
        
        internal int Count
        {
            get
            {
                return this.keyValuePair.Count;
            }
        }

        private int GetId()
        {
            if (this.location == null)
            {
                return 0;
            }

            string[] locationAsArr = this.location.ToString().Split("/".ToCharArray());
            return Convert.ToInt32(locationAsArr[locationAsArr.Length - 1]);
        }

        public virtual BaseEntity SetProperty(string propertyName, string propertyValue)
        {
            if (this.GetLocation() == null || this.Id <= 0)
            {
                throw new InvalidNodeException();
            }

            this.AddProperty(propertyName, propertyValue);
            return this.baseRepo.SetProperty(this, propertyName);
        }

        public virtual BaseEntity UpdateProperties(string propertyName, string propertyValue)
        {
            if (this.GetLocation() == null || this.Id <= 0)
            {
                throw new InvalidNodeException();
            }

            this.keyValuePair[propertyName] = propertyValue;
            JObject props = new JObject { { propertyName, new JValue(propertyValue) } };
            this.baseRepo.UpdateProperty(this, props.ToString());
            return this;
        }

        public BaseEntity AddProperty(string propertyName, string propertyValue)
        {
            if (this.keyValuePair.ContainsKey(propertyName))
            {
                this.keyValuePair[propertyName] = propertyValue;
            }
            else
            {
                this.keyValuePair.Add(propertyName, propertyValue);
            }

            return this;
        }

        public string GetProperty(string key)
        {
            if (this.keyValuePair.ContainsKey(key) == false)
            {
                return string.Empty;
            }

            return this.keyValuePair[key].ToString();
        }
        
        internal virtual void SetLocation(Uri location)
        {
            this.location = location;
        }

        public Uri GetLocation()
        {
            return this.location;
        }

        public virtual string GetProperties()
        {
            NodeParser nodeParser = new NodeParser(this.keyValuePair);
            return nodeParser.EntityToJson();
        }
    }
}
