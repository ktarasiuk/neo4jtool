﻿namespace Neo4j.Tool.Entity
{
    using System;
    using System.Collections.Generic;

    using Neo4j.Tool.Parser;
    using Neo4j.Tool.Persistence;
    using Neo4j.Tool.Traversal;

    using Net.Graph.Neo4JD.Exceptions;

    public class Node : BaseEntity
    {
        readonly NodeRepository nodeRepo;

        readonly RelationshipRepository relationShipRepo;

        public List<string> Label { get; set; }

        internal Node()
        {
            this.Label = new List<string>(1);
            this.nodeRepo = new NodeRepository();
            this.relationShipRepo = new RelationshipRepository();
            base.SetRepository(this.nodeRepo);
        }

        internal Node(RequestResult result)
            : this()
        {
            this.SetResult(result);
        }

        public Node(NodeRepository nodeRepo, RelationshipRepository relationShipRepo)
        {
            this.Label = new List<string>(1);
            this.nodeRepo = nodeRepo;
            this.relationShipRepo = relationShipRepo;
            base.SetRepository(this.nodeRepo);
        }

        internal override void SetLocation(Uri location)
        {
            if (location.ToString().Contains("relationship"))
            {
                throw new InvalidCastException(string.Format("Unable to cast Relationship to Node. The type is a Relationship, Location: {0}", location));
            }

            base.SetLocation(location);
        }

        public override BaseEntity SetProperty(string propertyName, string propertyValue)
        {
            base.AddProperty(propertyName, propertyValue);
            return this.nodeRepo.SetProperty(this, propertyName);
        }
        
        internal void SetResult(RequestResult result)
        {
            NodeParser nodeParser = new NodeParser(this.keyValuePair);
            nodeParser.JsonToEntity(result, this);
        }

        public void SetLabel(RequestResult result)
        {
            NodeParser nodeParser = new NodeParser(null);
            nodeParser.JsonToLabels(result, this);
        }

        internal Relationship CreateRelationshipTo(Node node, string relationShipType)
        {
            if ((this.GetLocation() == null) || (node.GetLocation() == null))
            {
                throw new Exception("Parent or child node is not created. Create both node before creating the relation");
            }

            Relationship relationShip = new Relationship(this, node, relationShipType);
            return this.relationShipRepo.CreateRelationship(this, relationShip);
        }

        public IList<Node> In()
        {
            return this.GetRelationshipNodes("in", "start");
        }

        public IList<Node> Out()
        {
            return this.GetRelationshipNodes("out", "end");
        }

        private IList<Node> GetRelationshipNodes(string direction, string element)
        {
            if (this.GetLocation() == null || this.Id <= 0)
                throw new InvalidNodeException();

            RequestResult result = this.nodeRepo.GetRelatedNodes(this, direction);

            return this.nodeRepo.CreateNodeArray(element, result);
        }

        public IList<Relationship> GetAllPathsTo(Node endNode)
        {
            RequestResult result = this.nodeRepo.GetRelatedNodes(this, "out");
            return this.nodeRepo.GetAllPathsTo(endNode, result);
        }

        internal IList<Node> Filter(IQuery query)
        {
            RequestResult result = null;
            if (query.GetType() == typeof(RestTraversal))
            {
                result = this.nodeRepo.GetRestExecutionResult(this, query.ToString());
            }

            return this.nodeRepo.CreateNodeArray("self", result);
        }
    }
}