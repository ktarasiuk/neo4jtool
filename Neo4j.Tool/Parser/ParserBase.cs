﻿namespace Neo4j.Tool.Parser
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Neo4j.Tool.Entity;
    using Neo4j.Tool.Persistence;

    using Newtonsoft.Json.Linq;

    public class ParserBase
    {
        private readonly Dictionary<string, object> keyValuePair;

        private string responseData = string.Empty;

        public ParserBase(Dictionary<string,object> keyValuePair)
        {
            this.keyValuePair = keyValuePair;
        }

        public ParserBase()
        {
            this.keyValuePair = new Dictionary<string, object>();
        }

        protected string GetResponseData()
        {
            return this.responseData;
        }

        public virtual void JsonToEntity(RequestResult result, BaseEntity entity)
        {
            this.responseData = result.GetResponseData();
            JObject jobject = JObject.Parse(this.responseData);
            JToken token;
            jobject.TryGetValue("data", out token);

            foreach (string[] s in token.Select(t => t.ToString().Replace("\"", string.Empty).Split(":".ToCharArray())))
            {
                this.keyValuePair.Add(s[0].Trim(), s[1].Trim());
            }

            entity.SetLocation(new Uri(jobject["self"].ToString()));
        }

        public virtual string EntityToJson()
        {
            JObject props = new JObject();
            foreach (var key in this.keyValuePair.Keys)
            {
                props.Add(key, new JValue(this.keyValuePair[key]));
            }

            return props.ToString();
        }
    }
}
