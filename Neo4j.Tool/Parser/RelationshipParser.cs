﻿namespace Neo4j.Tool.Parser
{
    using System.Collections.Generic;

    using Neo4j.Tool.Entity;
    using Neo4j.Tool.Persistence;

    using Newtonsoft.Json.Linq;

    public class RelationshipParser : ParserBase
    {
        public RelationshipParser(Dictionary<string, object> keyValuePair)
            : base(keyValuePair)
        {
        }

        public override void JsonToEntity(RequestResult result, BaseEntity entity)
        {
            base.JsonToEntity(result, entity);
            JObject jobject = JObject.Parse(GetResponseData());
            ((Relationship)entity).SetVertices(jobject["start"].ToString(), jobject["end"].ToString());
            ((Relationship)entity).SetType(jobject["type"].ToString());
        }
    }
}
