﻿namespace Neo4j.Tool.Parser
{
    using System.Collections.Generic;

    using Neo4j.Tool.Entity;
    using Neo4j.Tool.Persistence;

    using Newtonsoft.Json;

    public class NodeParser : ParserBase
    {
        public NodeParser(Dictionary<string,object> keyValuePair)
            : base(keyValuePair)
        {

        }

        public NodeParser()
        {
            
        }

        public void JsonToLabels(RequestResult result, Node entity)
        {
            var resultString = result.GetResponseData();
            var labels = JsonConvert.DeserializeObject<List<string>>(resultString);

            if (labels != null)
            {
                entity.Label = labels;
            }
        }
    }
}
