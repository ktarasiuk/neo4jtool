﻿namespace Net.Graph.Neo4JD.Exceptions
{
    using System;

    using Neo4j.Tool.Entity;

    public class NodeDeleteException : Exception
    {
        private readonly Node nodeDeleteFailed;

        public NodeDeleteException(Node nodeDeleteFailed)
        {
            this.nodeDeleteFailed = nodeDeleteFailed;
        }

        public override string Message
        {
            get
            {
                return string.Format("The node with id [{0}] cannot be deleted. Check that the node is orphaned before deletion.", this.nodeDeleteFailed.Id);
            }
        }
    }
}
