﻿namespace Net.Graph.Neo4JD.Exceptions
{
    using System;

    public class NodeNotFoundException : Exception
    {
        private readonly int unknownId;

        private readonly string unknownLocation;

        public NodeNotFoundException(Exception ex, int unknownNodeId)
        {
            if (ex.Message.Contains("404"))
            {
                this.unknownId = unknownNodeId;
            }
            else
            {
                throw ex;
            }
        }

        public NodeNotFoundException(Exception ex, string unknownNodeLocation)
        {
            if (ex.Message.Contains("404"))
            {
                this.unknownLocation = unknownNodeLocation;
            }
            else
            {
                throw ex;
            }
        }

        public override string Message
        {
            get
            {
                if (string.IsNullOrEmpty(this.unknownLocation))
                {
                    return string.Format("Cannot find node with id [{0}] in database.", this.unknownId);
                }
                else
                {
                    return string.Format("Cannot find node with address [{0}] in database.", this.unknownLocation);
                }
            }
        }
    }
}
