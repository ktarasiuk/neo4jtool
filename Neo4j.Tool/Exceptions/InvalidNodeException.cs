﻿namespace Net.Graph.Neo4JD.Exceptions
{
    using System;

    public class InvalidNodeException : Exception
    {
        public override string Message
        {
            get
            {
                return "Location is null or invalid. Get a valid Node/Relationship from db to perform this operation.";
            }
        }
    }
}
