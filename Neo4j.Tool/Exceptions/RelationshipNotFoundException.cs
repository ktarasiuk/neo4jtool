﻿namespace Net.Graph.Neo4JD.Exceptions
{
    using System;

    public class RelationshipNotFoundException : Exception
    {
         private readonly int unknownId;

        private readonly string unknownLocation;

        public RelationshipNotFoundException(Exception ex, int unknownRelationshipId)
        {
            if (ex.Message.Contains("404"))
            {
                this.unknownId = unknownRelationshipId;
            }
            else
            {
                throw ex;
            }
        }

        public RelationshipNotFoundException(Exception ex, string unknownRelationshipLocation)
        {
            if (ex.Message.Contains("404"))
            {
                this.unknownLocation = unknownRelationshipLocation;
            }
            else
            {
                throw ex;
            }
        }

        public override string Message
        {
            get
            {
                if (string.IsNullOrEmpty(this.unknownLocation))
                {
                    return string.Format("Cannot find relationship with id [{0}] in database.", this.unknownId);
                }
                else
                {
                    return string.Format("Cannot find relationship with address [{0}] in database.", this.unknownLocation);
                }
            }
        }
    }
}