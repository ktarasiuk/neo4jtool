﻿namespace Neo4j.Tool.Database
{
    using Neo4j.Tool.Entity;

    public class NodeEntry
    {
        public Node Node { get; private set; }

        public OperationType Operation { get; private set; }

        public NodeEntry(Node node, OperationType operation)
        {
            this.Node = node;
            this.Operation = operation;
        }
    }
}