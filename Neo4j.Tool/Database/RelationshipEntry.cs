﻿namespace Neo4j.Tool.Database
{
    using Neo4j.Tool.Entity;

    public class RelationshipEntry
    {
         public Relationship Relationship { get; private set; }

        public OperationType Operation { get; private set; }

        public RelationshipEntry(Relationship relationship, OperationType operation)
        {
            this.Relationship = relationship;
            this.Operation = operation;
        }
    }
}