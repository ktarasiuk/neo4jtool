﻿namespace Neo4j.Tool.Database
{
    using System;
    using System.Collections.Generic;
    using System.Net;

    using Neo4j.Tool.Entity;
    using Neo4j.Tool.Persistence;
    using Neo4j.Tool.Traversal;

    using Net.Graph.Neo4JD.Exceptions;

    public class DatabaseContext
    {
        private readonly List<NodeEntry> nodeStorage;

        private readonly List<RelationshipEntry> relationshipStorage;

        private readonly NodeRepository nodeRepo;

        private readonly RelationshipRepository relationShipRepo;

        public DatabaseContext(string address)
        {
            Settings.SetBaseUri(address);

            this.nodeRepo = new NodeRepository();
            this.relationShipRepo = new RelationshipRepository();

            this.nodeStorage = new List<NodeEntry>();
            this.relationshipStorage = new List<RelationshipEntry>();
        }

        public Node CreateNode()
        {
            Node node = new Node(this.nodeRepo, this.relationShipRepo);
            this.nodeStorage.Add(new NodeEntry(node, OperationType.Create));
            return node;
        }

        public Node GetNodeById(int id)
        {
            try
            {
                return this.nodeRepo.Get(id.ToString());
            }
            catch (WebException ex)
            {
                throw new NodeNotFoundException(ex, id);
            }
        }

        public Node GetNode(string location)
        {
            try
            {
                return this.nodeRepo.Get(new Uri(location));
            }
            catch (WebException ex)
            {
                throw new NodeNotFoundException(ex, location);
            }
        }

        public Relationship GetRelationshipById(int id)
        {
            try
            {
                return this.relationShipRepo.GetRelationship(id.ToString());
            }
            catch (WebException ex)
            {
                throw new RelationshipNotFoundException(ex, id);
            }
        }

        public Relationship GetRelationship(string location)
        {
            try
            {
                return this.relationShipRepo.GetRelationship(new Uri(location));
            }
            catch (WebException ex)
            {
                throw new RelationshipNotFoundException(ex, location);
            }
        }

        public Relationship CreateRelationship(Node from, Node to, string type)
        {
            Relationship relationship = new Relationship(from, to, type);
            this.relationshipStorage.Add(new RelationshipEntry(relationship, OperationType.Create));
            return relationship;
        }

        public void DeleteNode(int id)
        {
            var node = this.GetNodeById(id);
            this.DeleteNode(node);
        }

        public void DeleteNode(Node node)
        {
            if (node.GetLocation() == null || node.Id <= 0)
            {
                throw new InvalidNodeException();
            }

            this.nodeStorage.Add(new NodeEntry(node, OperationType.Delete));
        }

        public void DeleteRelationship(int id)
        {
            var relationship = this.GetRelationshipById(id);
            this.DeleteRelationship(relationship);
        }

        public void DeleteRelationship(Relationship relationship)
        {
            if (relationship.GetLocation() == null || relationship.Id <= 0)
            {
                throw new NullReferenceException("Location is null. The relationship might not be valid, get a valid relation from the db.");
            }

            this.relationshipStorage.Add(new RelationshipEntry(relationship, OperationType.Delete));
        }

        public IList<Node> Traverse(Node startNode, RestTraversal traversal)
        {
            return startNode.Filter(traversal);
        }

        public RestTraversal Query()
        {
            return new RestTraversal();
        }

        public void Commit()
        {
            this.nodeStorage.ForEach(
                entity =>
                {
                    switch (entity.Operation)
                    {
                        case OperationType.Create:
                            this.nodeRepo.CreateNode(entity.Node);
                            break;
                        case OperationType.Delete:
                            this.nodeRepo.Delete(entity.Node);
                            break;
                    }
                });

            this.relationshipStorage.ForEach(
                relation =>
                {
                    switch (relation.Operation)
                    {
                        case OperationType.Create:
                            this.relationShipRepo.Create(relation.Relationship);
                            break;
                        case OperationType.Delete:
                            this.relationShipRepo.Delete(relation.Relationship);
                            break;
                    }
                });

            this.nodeStorage.Clear();
            this.relationshipStorage.Clear();
        }
    }
}