﻿namespace Neo4j.Tool.Database
{
    using System;

    internal class Settings
    {
        private static Uri baseUri;

        public static void SetBaseUri(string baseUri)
        {
            if (baseUri.EndsWith("/") == false)
            {
                baseUri = baseUri + "/";
            }

            Settings.baseUri = new Uri(baseUri);
        }

        public static Uri GetBaseUri()
        {
            if (baseUri == null)
            {
                throw new NullReferenceException("The base URI is not set. Set base Uri by calling GraphEnvironment.SetBaseUri()");
            }

            return baseUri;
        }
    }
}