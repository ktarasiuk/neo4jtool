﻿namespace Neo4j.Tool.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public static class ListExtension
    {
        public static string ToStringContent<T>(this List<T> list)
        {
            if (!list.Any())
            {
                return null;
            }

            StringBuilder sb = new StringBuilder();
            list.ForEach(item => sb.Append(item.ToString() + ", "));
            sb.Remove(sb.Length - 2, 2);

            return sb.ToString();
        }
    }
}