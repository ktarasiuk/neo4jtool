﻿namespace Neo4j.Tool.Traversal
{
    using Newtonsoft.Json.Linq;

    public class Filter : RestBasePipe
    {
        private readonly PropertyFilter filter;

        public Filter(PropertyFilter propertyFilter)
        {
            this.filter = propertyFilter;
        }

        public override object GetJson()
        {
            return new JProperty("return_filter", this.filter.GetJsonObject());
        }
    }
}
