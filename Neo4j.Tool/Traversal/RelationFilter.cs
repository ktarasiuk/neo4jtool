﻿namespace Neo4j.Tool.Traversal
{
    using Newtonsoft.Json.Linq;

    public class RelationFilter:RestBasePipe
    {
        private readonly RelationshipDirection _direction;
        private readonly string _relationShipTypeFilter;
        public RelationFilter(RelationshipDirection relationShipDirection, string relationShipTypeFilter)
        {
            this._direction=relationShipDirection;
            this._relationShipTypeFilter = relationShipTypeFilter;
        }

        public override object GetJson()
        {
            JObject relationShip = new JObject();
            string direction = this._direction.ToString().Split("_".ToCharArray())[0];

            relationShip.Add("direction", new JValue(direction));
            relationShip.Add("type", new JValue(this._relationShipTypeFilter));

            return relationShip;
        }
    }
}
