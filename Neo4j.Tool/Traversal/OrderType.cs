﻿namespace Neo4j.Tool.Traversal
{
    public enum OrderType
    {
        breadth_first = 0,

        depth_first = 1
    }
}