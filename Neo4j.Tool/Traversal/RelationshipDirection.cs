﻿namespace Neo4j.Tool.Traversal
{
    public enum RelationshipDirection
    {
        in_direction = 0,

        out_direction = 1,

        all_direction = 2
    }
}