﻿namespace Neo4j.Tool.Traversal
{
    public interface IQuery
    {
        string ToString();
    }
}
