﻿namespace Neo4j.Tool.Traversal
{
    using Newtonsoft.Json.Linq;

    public class PropertyFilter
    {
        private string propertyName=string.Empty;

        private string propertyValue = string.Empty;

        private string selectCriteria = string.Empty;

        private string userDefinedBody = string.Empty;

        public PropertyFilter Property(string propertyName)
        {
            this.propertyName = propertyName;
            return this;
        }

        public PropertyFilter Contains(string propertyValue)
        {
            this.propertyValue = propertyValue;
            this.selectCriteria = "contains";
            return this;
        }

        public PropertyFilter UserDefinedFilter(string filter)
        {
            this.userDefinedBody = filter;
            return this;
        }

        public PropertyFilter Equals(string propertyValue)
        {
            this.propertyValue = propertyValue;
            this.selectCriteria = "equals";
            return this;
        }

        internal object GetJsonObject()
        {
            JObject returnFilter = new JObject();
            string filter = string.IsNullOrEmpty(this.userDefinedBody) == false
                                ? this.userDefinedBody
                                : string.Format(
                                    "position.endNode().getProperty('{0}').toLowerCase().{1}('{2}')",
                                    this.propertyName,
                                    this.selectCriteria,
                                    this.propertyValue);
            returnFilter.Add("body", new JValue(filter));
            returnFilter.Add("language", new JValue("javascript"));

            return returnFilter;
        }
    }
}
