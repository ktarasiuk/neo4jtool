﻿namespace Neo4j.Tool.Traversal
{
    using Newtonsoft.Json.Linq;

    public class Uniqueness : RestBasePipe
    {
        private readonly UniquenessType uniquenessUserSelected;

        public Uniqueness(UniquenessType uniqueness)
        {
            this.uniquenessUserSelected = uniqueness;
        }

        public override object GetJson()
        {
            return new JProperty("uniqueness", this.uniquenessUserSelected.ToString());
        }
    }
}
