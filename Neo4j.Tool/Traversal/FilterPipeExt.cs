﻿namespace Neo4j.Tool.Traversal
{
    using System;

    public class FilterPipeExt : RestBasePipe
    {
        private Func<PropertyFilter, object> _filter;

        public FilterPipeExt(Func<PropertyFilter,object> property)
        {
            this._filter = property;
        }

        public override object GetJson()
        {
            return null;
        }
    }
}
