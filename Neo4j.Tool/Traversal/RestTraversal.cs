﻿namespace Neo4j.Tool.Traversal
{
    using System.Collections.Generic;

    using Newtonsoft.Json.Linq;

    public class RestTraversal : IQuery
    {
        readonly IList<RestBasePipe> restPipes;

        private RelationShipFilter relationShipFilter;
        internal RestTraversal()
        {
            this.restPipes = new List<RestBasePipe>();
        }

        private RestTraversal Add(RestBasePipe pipe)
        {
            this.restPipes.Add(pipe);
            return this;
        }

        public RestTraversal Order(OrderType order)
        {
            return this.Add(new Order(order));
        }

        public RestTraversal Filter(PropertyFilter filter)
        {
            return this.Add(new Filter(filter));
        }

        public RestTraversal RelationShips(RelationshipDirection relationShipDirection, string relationShipType)
        {
            if (this.relationShipFilter == null)
            {
                this.relationShipFilter = new RelationShipFilter();
                this.Add(this.relationShipFilter);
            }

            this.relationShipFilter.Add(new RelationFilter(relationShipDirection,relationShipType));
            return this;
        }

        public RestTraversal Uniqueness(UniquenessType uniqueness)
        {
            return this.Add(new Uniqueness(uniqueness));
        }

        public RestTraversal MaxDepth(int maxDepth)
        {
            return this.Add(new MaxDepth(maxDepth));
        }

        public override string ToString()
        {
            JObject restQuery = new JObject();

            foreach (RestBasePipe pipe in this.restPipes)
            {
                restQuery.Add(pipe.GetJson());
            }

            return restQuery.ToString();
        }
    }
}
