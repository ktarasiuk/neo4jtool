﻿namespace Neo4j.Tool.Traversal
{
    using System.Collections.Generic;

    using Newtonsoft.Json.Linq;

    public class RelationShipFilter : RestBasePipe
    {
        private readonly IList<RestBasePipe> relationShips;

        public RelationShipFilter()
        {
            this.relationShips = new List<RestBasePipe>();
        }

        public RelationShipFilter(List<RelationFilter> relationFilters)
            : this()
        {
            if (relationFilters != null)
            {
                this.relationShips = (IList<RestBasePipe>)relationFilters;
            }
        }

        public RelationShipFilter Add(RelationFilter relationFilter)
        {
            this.relationShips.Add(relationFilter);
            return this;
        }

        public override object GetJson()
        {
            JArray relationships = new JArray();
            foreach (RestBasePipe relationFilter in this.relationShips)
            {
                relationships.Add(relationFilter.GetJson());
            }

            return new JProperty("relationships", relationships);
        }
    }
}
