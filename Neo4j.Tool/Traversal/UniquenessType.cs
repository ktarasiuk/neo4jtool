﻿namespace Neo4j.Tool.Traversal
{
    public enum UniquenessType
    {
        node_global = 0,

        none = 1,

        relationship_global = 2,

        node_path = 3,

        relationship_path = 4
    }
}