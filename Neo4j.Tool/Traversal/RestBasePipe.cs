﻿namespace Neo4j.Tool.Traversal
{
    using Newtonsoft.Json.Linq;

    public abstract class RestBasePipe
    {
        private string pipeName;

        private object value;

        protected void SetPipeName(string pipeName)
        {
            this.pipeName = pipeName;
        }

        protected void SetPipeValue(object pipeValue)
        {
            this.value = pipeValue;
        }

        public virtual object GetJson()
        {
            return null;
        }

        public string GetPipeName()
        {
            return this.pipeName;
        }

        public string GetPipeValue()
        {
            return this.value.ToString();
        }

        public override string ToString()
        {
            JObject props = new JObject();
            props.Add(this.pipeName, new JValue(this.value));
            return props.ToString();
        }
    }
}
