﻿namespace Neo4j.Tool.Traversal
{
    using Newtonsoft.Json.Linq;
    
    public class Order : RestBasePipe
    {
        public Order(OrderType searchType)
        {
            base.SetPipeName("order");
            base.SetPipeValue(searchType.ToString());
        }

        public override object GetJson()
        {
            return new JProperty(GetPipeName(), GetPipeValue());
        }
    }
}
