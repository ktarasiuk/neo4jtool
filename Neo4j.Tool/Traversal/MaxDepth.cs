﻿namespace Neo4j.Tool.Traversal
{
    using Newtonsoft.Json.Linq;

    public class MaxDepth : RestBasePipe
    {
        private readonly int maxDepth;
        
        public MaxDepth(int maxDepth)
        {
            this.maxDepth = maxDepth;
        }

        public override object GetJson()
        {
            return new JProperty("max_depth", this.maxDepth);
        }
    }
}
