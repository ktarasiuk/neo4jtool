﻿namespace Neo4j.Tool.Persistence
{
    using System;
    using System.Net;
    using System.Text;

    public class GraphRequest
    {
        public RequestResult Post(string method, Uri uri, string properties)
        {
            if (uri == null)
            {
                throw new ArgumentNullException("uri");
            }

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Method = method;

            if (!string.IsNullOrWhiteSpace(properties))
            {
                this.AddBody(httpWebRequest, properties);
            }

            Console.Write("{0} [{1}] ... ", httpWebRequest.Address, httpWebRequest.Method);

            var response = httpWebRequest.GetResponse();
            var result = new RequestResult(response);

            Console.WriteLine((response as HttpWebResponse).StatusCode);
            return result;
        }

        private void AddBody(HttpWebRequest request, string nodeData)
        {
            request.ContentType = "application/json";
            byte[] bytes = Encoding.UTF8.GetBytes(nodeData);
            request.ContentLength = bytes.Length;
            using (var reqStream = request.GetRequestStream())
            {
                reqStream.Write(bytes, 0, bytes.Length);
            }
        }
    }
}