﻿namespace Neo4j.Tool.Persistence
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;

    public class RequestResult
    {
        private readonly WebResponse response;
        public RequestResult(WebResponse response)
        {
            this.response = response;
        }

        public Uri GetLocation()
        {
            return new Uri(this.response.Headers["Location"]);
        }

        public string GetResponseData()
        {
            var webResponse = this.response;

            if (webResponse != null)
            {
                var readStream = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8);
                var buffer = new char[256];

                var stringBuilder = new StringBuilder();
                int count = readStream.Read(buffer, 0, 256);
                while (count > 0)
                {
                    var str = new string(buffer, 0, count);
                    stringBuilder.Append(str);
                    count = readStream.Read(buffer, 0, 256);
                }
                var responseData = stringBuilder.ToString();
                
                return responseData;
            }

            return null;
        }
    }
}