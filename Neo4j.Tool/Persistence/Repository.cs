﻿namespace Neo4j.Tool.Persistence
{
    using System;

    using Neo4j.Tool.Entity;
    using Neo4j.Tool.Helpers;

    using Net.Graph.Neo4JD.Exceptions;

    public class Repository
    {
        protected readonly GraphRequest graphRequest;

        public Repository()
        {
            this.graphRequest = new GraphRequest();
        }

        public virtual void Delete(BaseEntity entity)
        {
            try
            {
                this.graphRequest.Post(RequestType.DELETE, entity.GetLocation(), null);
            }
            catch (Exception)
            {
                if (entity.GetType() == typeof(Node))
                {
                    throw new NodeDeleteException(entity as Node);
                }
                
                throw;
            }
        }

        public virtual BaseEntity SetProperty(BaseEntity entity, string propertyName)
        {
            var uri = UriHelper.ConcatUri(entity.GetLocation(), "/properties", propertyName);
            this.graphRequest.Post(RequestType.PUT, uri, string.Format(@"""{0}""", entity.GetProperty(propertyName)));
            return entity;
        }

        public virtual BaseEntity UpdateProperty(BaseEntity entity, string propertyToUpdate)
        {
            var uri = UriHelper.ConcatUri(entity.GetLocation(), "/properties");
            this.graphRequest.Post(RequestType.PUT, uri, propertyToUpdate);

            return entity;
        }
    }
}