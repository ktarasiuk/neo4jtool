﻿namespace Neo4j.Tool.Persistence
{
    public class RequestType
    {
        public const string PUT = "PUT";

        public const string GET = "GET";

        public const string POST = "POST";

        public const string DELETE = "DELETE";
    }
}