﻿namespace Neo4j.Tool.Persistence
{
    using System;

    using Neo4j.Tool.Database;
    using Neo4j.Tool.Entity;
    using Neo4j.Tool.Helpers;

    public class RelationshipRepository : Repository
    {
        public Relationship GetRelationship(string relationShipId)
        {
            var uri = UriHelper.ConcatUri(Settings.GetBaseUri(), "db/data/relationship/" + relationShipId);
            return this.GetRelationship(uri);
        }

        public Relationship GetRelationship(Uri uri)
        {
            var result = graphRequest.Post(RequestType.GET, uri, null);
            Relationship relationship = new Relationship(result);
            return relationship;
        }

        public Relationship CreateRelationship(Node parent, Relationship relationShip)
        {
            var result = graphRequest.Post(RequestType.POST, new Uri(string.Concat(parent.GetLocation(), @"/relationships")), relationShip.GetProperties());
            relationShip.SetLocation(result.GetLocation());
            return relationShip;
        }

        public Relationship Create(Relationship relationShip)
        {
            var result = graphRequest.Post(RequestType.POST, new Uri(string.Concat(relationShip.StartNode.GetLocation(), @"/relationships")), relationShip.GetProperties());
            relationShip.SetLocation(result.GetLocation());
            return relationShip;
        }
    }
}