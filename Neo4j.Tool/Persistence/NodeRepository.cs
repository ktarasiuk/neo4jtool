﻿namespace Neo4j.Tool.Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Neo4j.Tool.Database;
    using Neo4j.Tool.Entity;
    using Neo4j.Tool.Helpers;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class NodeRepository : Repository
    {
        private readonly RelationshipRepository relationShipRepo;

        public NodeRepository()
        {
            this.relationShipRepo = new RelationshipRepository();
        }

        public Node Get(Uri nodeUri)
        {
            Node node = new Node();
            node.SetLocation(nodeUri);
            return this.Get(node);
        }

        public Node Get(string nodeId)
        {
            var uri = UriHelper.ConcatUri(Settings.GetBaseUri(), "db/data/node/" + nodeId);
            Node node = new Node();
            node.SetLocation(uri);
            return this.Get(node);
        }

        public Node Get(Node node)
        {
            var result = graphRequest.Post(RequestType.GET, node.GetLocation(), null);
            node.SetResult(result);

            var labelResult = graphRequest.Post(
                RequestType.GET,
                UriHelper.ConcatUri(Settings.GetBaseUri(), "db/data/node/" + node.Id + "/labels"),
                null);

            node.SetLabel(labelResult);
            return node;
        }

        public Node CreateNode(Node node)
        {
            var uri = UriHelper.ConcatUri(Settings.GetBaseUri(), "db/data/node");
            var result = graphRequest.Post(RequestType.POST, uri, node.GetProperties());
            node.SetLocation(result.GetLocation());

            if (node.Label.Any())
            {
                var uriNode = UriHelper.ConcatUri(Settings.GetBaseUri(), "db/data/node/" + node.Id + "/labels");
                graphRequest.Post(RequestType.POST, uriNode, "\"" + node.Label.ToStringContent() + "\"");
            }

            return node;
        }

        public RequestResult GetRelatedNodes(Node node, string direction)
        {
            RequestResult result = graphRequest.Post(RequestType.GET, new Uri(string.Concat(node.GetLocation(), @"/relationships/", direction)), null);
            return result;
        }

        public RequestResult GetRestExecutionResult(Node node, string query)
        {
            var uri = UriHelper.ConcatUri(node.GetLocation(), "/traverse/node");
            var result = graphRequest.Post(RequestType.POST, uri, query);
            return result;
        }

        public RequestResult GetGermlinExecutionResult(Node node, string query)
        {
            query = query.Replace("#id", node.Id.ToString());
            var uri = UriHelper.ConcatUri(Settings.GetBaseUri(), "db/data/ext/GremlinPlugin/graphdb/execute_script");
            var result = graphRequest.Post(RequestType.POST, uri, query);
            return result;
        }

        public IList<Node> CreateNodeArray(string element, RequestResult result)
        {
            IList<Node> childs = new List<Node>();
            JArray array = JArray.Parse(result.GetResponseData());
            foreach (var token in array)
            {
                Node node = new Node();
                node.SetLocation(new Uri(token[element].ToString()));
                node = this.Get(node);
                childs.Add(node);
            }
            return childs;
        }

        public IList<Relationship> GetAllPathsTo(Node toNode, RequestResult result)
        {
            IList<Relationship> relationShips = new List<Relationship>();
            JArray array = JArray.Parse(result.GetResponseData());
            foreach (var token in array)
            {
                Node node = new Node();
                node.SetLocation(new Uri(token["end"].ToString()));
                if (node.Id == toNode.Id)
                {
                    Relationship relationShip = this.relationShipRepo.GetRelationship(new Uri(token["self"].ToString()));
                    relationShips.Add(relationShip);
                }
            }

            return relationShips;
        }
    }
}